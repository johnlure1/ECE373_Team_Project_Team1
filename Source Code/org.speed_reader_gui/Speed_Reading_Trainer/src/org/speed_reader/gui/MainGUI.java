package org.speed_reader.gui;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyledDocument;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.RoundRectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static java.time.temporal.ChronoUnit.MINUTES;
import org.speed_reader.data.*;
import org.speed_reader.gui.UserSelectionFrame.ActionType;

public class MainGUI extends JFrame {
	
//Styling Global Vars
/*------------------------------------------------------------------------------------------------------------------------------------*/	
	int WINDOW_WIDTH = DPIScaling.scaleInt(1000); 
	int WINDOW_HEIGHT = DPIScaling.scaleInt(900);
	Color defaultBackgroundColor = new Color(24,145,227);  	//rgb - light blue color
	Color defaultTextColor = new Color(90,90,95);       	//rgb - gray
	int defaultFontSize = DPIScaling.scaleInt(12);
	int emphasisFontSize = DPIScaling.scaleInt(18);
/*------------------------------------------------------------------------------------------------------------------------------------*/	
//Provides for dynamic font sizing of reading
	
	public int readingFontSize = DPIScaling.scaleInt(22);													//User definable font size for the reading
	
	//Empirical Reading Panel tuning parameters based on window_height -- until I find a better way.
	public int numReadingRows = (int)(WINDOW_HEIGHT/18*(12/readingFontSize));			//Number of Rows in the Reading Pane (char tall)
	public int numReadingCols = (int)(WINDOW_WIDTH/13*(1.05*12/readingFontSize));			//Number of Cols in the Reading Pane (char wide)
/*------------------------------------------------------------------------------------------------------------------------------------*/	
//User & Document variables
	public String userName = "Maria";
	private User currUser = null;
	public String userFileParentDir = "data";
	public String userFileLocation = "data/users.ser";
	public ArrayList<User> allUsers = null; // Initialized during readUsersFromFile()
	public Document currDoc = null;
	public LocalDateTime startTime = LocalDateTime.now(); // current time
	public int currWPM = 300;
	public int fastestWPMToday;
	
	ArrayList<String> documents = new ArrayList<String>();
/*------------------------------------------------------------------------------------------------------------------------------------*/	
//SWING GUI Global Objs
	private static final long serialVersionUID = 1L;
	MainGUI mainGUI; // So that the main object's self.x can be called in other scopes with mainGUI.x
	JFrame mainFrame;
	DocumentListPanel docListPanel;
		//Globally accessible JList in docListPanel
		public JList<String> docList;
		public DefaultListModel<String> model;
	
		//STYLE Elements within docListPanel

		public JScrollPane listScroller; 			//scrollbar for the JList
	
	DocumentStatisticsPanel docStatsPanel;
	KeyCommandTextPane textPane;					//text pane with key listener
	TextHighlighter textHighlighter = null;			//document-highlighting object
	WestPanel westPanel;
	SouthPanel southPanel;
	CenterPanel centerPanel;
	ArrayList<JMenuItem> userSpecificMenuItems = new ArrayList<JMenuItem>(); // Menu items enabled only when a user is logged in
	ArrayList<JMenuItem> docSpecificMenuItems = new ArrayList<JMenuItem>(); // Menu items enabled only when a document is open
/*------------------------------------------------------------------------------------------------------------------------------------*/	
				
	
	MainGUI(){
		//Instantiate new JFrame with Title
		super("Speed Reading Trainer");

		mainGUI = this;
		
		readUsersFromFile();
		
		//DEBUG ONLY:
		/*for (int i=0;i < 50; i++) {
			documents.add("C:\\texts\\Document"+i+".txt");
		}
		documents.add("D:\\Document.txt");*/
		
		//Set Default Design Parameters
		setLayout(new BorderLayout());
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				mainGUI.saveAndExit();
			}
		});
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setUndecorated(true);
		setShape(new RoundRectangle2D.Double(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, DPIScaling.scaleInt(10), DPIScaling.scaleInt(10)));
		getRootPane().setWindowDecorationStyle(JRootPane.FRAME);


		
		modifyColors();
		
		//Setup the Initial Internals
		buildMainFrame();
		
		//Set JFrame to be moveable
		DragListener drag = new DragListener();
		addMouseListener( drag );
		addMouseMotionListener( drag );
		
		//Disable user- and document-specific menu options.
		setUserSpecificMenuItemsEnabled(false);
		setDocSpecificMenuItemsEnabled(false);
		
		//Display
		setVisible(true);
		
		Object[] options = {
				"Create New User",
				"Sign In as Existing User"
		};
		int createNewUser = JOptionPane.showOptionDialog(
				this,
				"Welcome to Speed-Reading Trainer v1.0!",
				"Speed-Reading Trainer v1.0",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.PLAIN_MESSAGE,
				null,
				options,
				options[1]
		);
		if(createNewUser == JOptionPane.YES_OPTION){
			UserSelectionFrame userCreate = new UserSelectionFrame(ActionType.CREATE_NEW, this);
		} else {
			UserSelectionFrame userSignIn = new UserSelectionFrame(ActionType.SIGN_IN, this);
		}
		
	}
	
	private void setUserSpecificMenuItemsEnabled(boolean enabled){
		for(JMenuItem menuItem : userSpecificMenuItems){
			menuItem.setEnabled(enabled);
		}
	}
	
	private void setDocSpecificMenuItemsEnabled(boolean enabled){
		for(JMenuItem menuItem : docSpecificMenuItems){
			menuItem.setEnabled(enabled);
		}
	}
	
	public void setCurrUser(User currUser){
		if(this.currUser != currUser){
			startTime = LocalDateTime.now();
		}
		// Set currUser.
		this.currUser = currUser;
		// Update document-list panel's title and contents.
		documents.clear();
		model.clear();
		for(Document doc : currUser.getDocs()){
			documents.add(doc.getPath());
			addToDocList(doc.getPath());
		}
		docListPanel.setTitle(currUser.getName() +"'s Documents:");
		updateDocList();
		// Disable document-specific menu options.
		setDocSpecificMenuItemsEnabled(false);
		// Display empty document in document display area.
		currDoc = null;
		centerPanel.displayString("");
		// Enable user-specific menu options if currUser is not null.
		if(this.currUser != null){
			setUserSpecificMenuItemsEnabled(true);
		}
	}
	
	public void addDoc(Document doc){
		documents.add(doc.getPath());
		currUser.addDoc(doc);
		addToDocList(doc.getPath());
	}
	
	public User getCurrUser(){
		return currUser;
	}
	
	private void saveAndExit(){
		try {
			 writeUsersToFile();
			 System.exit(0); // Exit normally.
		} catch(IOException ex){
			ex.printStackTrace();
			Object[] options = {
					"Exit",
					"Cancel"
			};
			if(JOptionPane.showOptionDialog(
					null,
					"Could not save user data. Exit anyway?",
					"Saving Failed",
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.ERROR_MESSAGE,
					null,
					options,
					options[1]
			) == JOptionPane.OK_OPTION){
				System.exit(1); // Exit code 1 because of save failure.
			} else {
				// Cancel. (Do not exit.)
			}
		}
	}
	
	private void writeUsersToFile() throws IOException {
		File parentDir = new File(userFileParentDir);
		if(!parentDir.exists()){
			try {
				parentDir.mkdir();
			} catch(SecurityException e){
				throw new IOException();
			}
		}
		FileOutputStream	fOut	= null;
		ObjectOutputStream	objOut	= null;
		fOut	= new FileOutputStream(userFileLocation);
		objOut	= new ObjectOutputStream(fOut);
		int numUsersToWrite = allUsers.size();
		objOut.writeInt(numUsersToWrite);
		for(int i = 0; i < numUsersToWrite; i++){
			objOut.writeObject(allUsers.get(i));
		}
		objOut.close();
		fOut.close();
	}
	
	private void readUsersFromFile(){
		ArrayList<User>		users	= null;
		FileInputStream		fIn		= null;
		ObjectInputStream	objIn	= null;
		try {
			try {
				users	= new ArrayList<User>();
				fIn		= new FileInputStream(userFileLocation);
				objIn	= new ObjectInputStream(fIn);
				int numUsersToRead = objIn.readInt();
				for(int i = 0; i < numUsersToRead; i++){
					users.add((User)(objIn.readObject()));
				}
				objIn.close();
				fIn.close();
			} catch(ClassNotFoundException e){
				objIn.close();
				allUsers = new ArrayList<User>();
			}
		} catch(IOException e) {
			allUsers = new ArrayList<User>();
		}
		allUsers = users;
	}
	
	private void displayDocument(Document doc) throws FileNotFoundException {
		setDocSpecificMenuItemsEnabled(true);
		centerPanel.displayDocument(doc.getPath());
	}
	
	private void modifyColors() {
	       getRootPane().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, defaultBackgroundColor));  //set outside light blue border
	}
	
	private void buildMainFrame() {
		
		westPanel = new WestPanel();
		add(westPanel, BorderLayout.WEST);
		
		southPanel = new SouthPanel();
		add(southPanel,BorderLayout.SOUTH);
		
		centerPanel = new CenterPanel();
		add(centerPanel,BorderLayout.CENTER);
		
		pack();
		setVisible(true);
		
	}
	
	public void addToDocList(String docPath){
		model.addElement(docPath);
		docList.setModel(model);
		updateDocList();
	}
	
	public void updateDocList(){
		docListPanel.revalidate();
		docListPanel.repaint();
	}
	
	public void updateStats(){
		docStatsPanel.revalidate();
		docStatsPanel.repaint();
	}
	
	public void startReading(){
		synchronized(textHighlighter){
			System.out.println("mainGUI.startReading() received lock.");
			textHighlighter.setPaused(false);
			textHighlighter.interrupt();
		}
	}
	
	public void startReading(int wpm){
		synchronized(textHighlighter){
			System.out.println("mainGUI.startReading(int wpm) received lock.");
			textHighlighter.setWPM(wpm);
			textHighlighter.setPaused(false);
			textHighlighter.interrupt();
		}
	}
	
	public class DragListener extends MouseInputAdapter
	{
	    Point location;
	    MouseEvent pressed;
	 
	    public void mousePressed(MouseEvent me)
	    {
	        pressed = me;
	    }
	 
	    public void mouseDragged(MouseEvent me)
	    {
	        Component component = me.getComponent();
	        location = component.getLocation(location);
	        int x = location.x - pressed.getX() + me.getX();
	        int y = location.y - pressed.getY() + me.getY();
	        component.setLocation(x, y);
	     }
	}
	
/*	private class SplashScreen extends JPanel{

		private static final long serialVersionUID = 1L;
		
		public SplashScreen(){
			
		}
		
	}*/
	
	private class CenterPanel extends JPanel{

		private static final long serialVersionUID = 1L;
		
		private JScrollPane textScrollPane = null;
		private JLabel wpmLabel;
		
		private int width;
		private int height;

		CenterPanel(){
			
			this.width=(int)(WINDOW_WIDTH*.75);
			this.height=(int)(WINDOW_HEIGHT*.9);
			
			setLayout(new BorderLayout());
			this.setSize(width,height);
			this.setMaximumSize(new Dimension(width, (int)height));
			setBackground(defaultBackgroundColor);
			//setBorder(BorderFactory.createMatteBorder(0, 0, 4, 0, defaultBackgroundColor));
			
			wpmLabel = new JLabel("<html><p><font color=#5A5A5F size="+DPIScaling.scaleInt(5)+">Current Word Speed: " +"<font color=green>"+currWPM+"<font color=#5A5A5F> WPM</p></html>");
			wpmLabel.setForeground(defaultTextColor);
			wpmLabel.setBackground(Color.WHITE);
			wpmLabel.setOpaque(true);				//otherwise background goes unpainted
			wpmLabel.setMaximumSize(new Dimension(DPIScaling.scaleInt(100),(int)(height*.1)));
			wpmLabel.setHorizontalAlignment(SwingConstants.CENTER);
			wpmLabel.setVerticalAlignment(SwingConstants.CENTER);
			wpmLabel.setBorder(BorderFactory.createMatteBorder(4, 400, 4, 400, defaultBackgroundColor));
			
			add(wpmLabel, BorderLayout.NORTH);
			
			// Display empty document in document display area.
			currDoc = null;
			displayString("");
			
		}
		
		public void updateStats(){
			wpmLabel.setText("<html><p><font color=#5A5A5F size="+DPIScaling.scaleInt(5)+">Current Word Speed: " +"<font color=green>"+currWPM+"<font color=#5A5A5F> WPM</p></html>");
			wpmLabel.revalidate();
			wpmLabel.repaint();
		}
		
		public void displayDocument(String filePath) throws FileNotFoundException {
			displayString(loadDocument(filePath));
		}
		
		private String loadDocument(String filePath) throws FileNotFoundException {
			Scanner scanner = new Scanner(new File(filePath));
			String docStr = scanner.useDelimiter("\\Z").next();
			scanner.close();
			return docStr;
		}
		
		public void displayString(String docStr){
			if(textHighlighter != null){
				synchronized(textHighlighter){
					TextHighlighter.switchingThreads = true;
					textHighlighter.exit();
					try {
						textHighlighter.wait();
					} catch(InterruptedException e){
						e.printStackTrace();
					}
				}
				System.out.println("Exited textHighlighter thread.");
			}
			if(textPane != null){
				textPane.setTextHighlighter(null);
				textPane = null;
			}
			Pointer pointer;
			if(currUser != null){
				pointer = currUser.getPointer();
			} else {
				pointer = new Pointer();
			}
			DefaultStyledDocument doc = new DefaultStyledDocument();
			try {
				doc.insertString(0, docStr, pointer.getBaseStyle());
			} catch(BadLocationException e){}
			textPane = new KeyCommandTextPane(doc);
			textPane.setParentGUI(mainGUI);
			textHighlighter = new TextHighlighter(doc, textPane, docStr, pointer);
			TextHighlighter.switchingThreads = false;
			textPane.setTextHighlighter(textHighlighter);
			System.out.println("centerPanel.displayString(String docStr) received lock.");
			// textHighlighter.setWPM(500);
			textPane.setPreferredSize(new Dimension(width, height));
			JScrollPane newTextScrollPane = new JScrollPane(textPane);
			textPane.setEditable(false);
			newTextScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			Border border = BorderFactory.createMatteBorder(0, 0, 0, 0, defaultBackgroundColor);
			Border margin = new EmptyBorder(0,10,0,10);
			newTextScrollPane.setBorder(new CompoundBorder(border, margin));
			if(this.textScrollPane != null){
				this.remove(this.textScrollPane);
			}
			this.textScrollPane = newTextScrollPane;
			add(this.textScrollPane, BorderLayout.WEST);
			textPane.revalidate();
			textPane.repaint();
			System.out.println("About to call textHighlighter.start().");
			textHighlighter.start();
			System.out.println("Finished textHighlighter.start() call.");
		}
		
	}
	private class SouthPanel extends JPanel{

		private static final long serialVersionUID = 1L;

		SouthPanel(){
			setLayout(new FlowLayout());
			setBackground(Color.WHITE);
			setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, defaultBackgroundColor));
			
			JLabel southLabel = new JLabel("<html><p><font size="+DPIScaling.scaleInt(4)+">To <i>Decrease</i> or <i>Increase</i> Reading Speed, "+
											"Press <font size="+DPIScaling.scaleInt(6)+">&lsaquo<font size="+DPIScaling.scaleInt(4)+"> or <font size="+DPIScaling.scaleInt(6)+">&rsaquo<font size="+DPIScaling.scaleInt(4)+">.  "+
											"Press Spacebar to <i>Pause</i>.</p></html>", SwingConstants.CENTER);
			southLabel.setForeground(defaultTextColor);
			southLabel.setMaximumSize(new Dimension(WINDOW_WIDTH-DPIScaling.scaleInt(400),(int)(WINDOW_HEIGHT*.1)));
			add(southLabel);
		}
		
	}
	
	private class WestPanel extends JPanel{

		private static final long serialVersionUID = 1L;

		WestPanel(){
			//West Panel Styling
			setLayout(new BorderLayout());
			setSize((int)(WINDOW_WIDTH*.25),(int)(WINDOW_HEIGHT*.9));
		
			//Setup Menu
			Menu menu = new Menu();
			add(menu, BorderLayout.NORTH);
			
			//Create DocumentListPanel
			docListPanel = new DocumentListPanel("Documents:");
			add(docListPanel, BorderLayout.WEST);
			
			//Create DocumentListStatisticsPanel
			docStatsPanel = new DocumentStatisticsPanel();
			add(docStatsPanel, BorderLayout.SOUTH);
		
		}
		
	}
	private class Menu extends JMenuBar{
		
		private static final long serialVersionUID = 1L;

		Menu() {
			super();
			setSize(WINDOW_WIDTH,(int)(WINDOW_HEIGHT*.1));
			setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, defaultBackgroundColor));
			
			//Set Default Location/Size Properties
			setBackground(defaultBackgroundColor);
			
			//File Menu
			JMenu fileMenu = new JMenu("File");
			fileMenu.setForeground(Color.WHITE);
			fileMenu.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			
			JMenuItem openNewDocument = new JMenuItem("Open New Document (*.txt)");
			openNewDocument.setForeground(defaultTextColor);
			openNewDocument.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			userSpecificMenuItems.add(openNewDocument);
			
			JMenuItem saveCurrentPlace = new JMenuItem("Save Current Place");
			saveCurrentPlace.setForeground(defaultTextColor);
			saveCurrentPlace.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			docSpecificMenuItems.add(saveCurrentPlace);
			
			JMenuItem resetDocumentStatistics = new JMenuItem("Reset Document Statistics");
			resetDocumentStatistics.setForeground(defaultTextColor);
			resetDocumentStatistics.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			docSpecificMenuItems.add(resetDocumentStatistics);
			
			JMenuItem exit = new JMenuItem("Exit");
			exit.setForeground(defaultTextColor);
			exit.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			
			//Setup Listeners
			openNewDocument.addActionListener((ActionEvent event) -> {
				System.out.println("Open New Document (*.txt)");
				OpenNewFile f = new OpenNewFile(); //user input box for new document
			});
			saveCurrentPlace.addActionListener((ActionEvent event) -> {
				System.out.println("Save Current Place");
				//TODO
			});
			resetDocumentStatistics.addActionListener((ActionEvent event) -> {
				System.out.println("Reset Document Statistics");
				//TODO
			});
			exit.addActionListener((ActionEvent event) -> {
				mainGUI.saveAndExit();
			});
		
			fileMenu.add(openNewDocument);
			fileMenu.add(saveCurrentPlace);
			fileMenu.add(resetDocumentStatistics);
			fileMenu.add(exit);
			
			this.add(fileMenu);
		
			//User Menu
			JMenu userMenu = new JMenu("User");
			userMenu.setForeground(Color.WHITE);
			userMenu.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));

			JMenuItem selectExistingUser = new JMenuItem("Select Existing User Profile");
			selectExistingUser.setForeground(defaultTextColor);
			selectExistingUser.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));

			JMenuItem createNewUser = new JMenuItem("Create New User");
			createNewUser.setForeground(defaultTextColor);
			createNewUser.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));

			JMenuItem resetAllDocumentStatistics = new JMenuItem("Reset All Document Statistics");
			resetAllDocumentStatistics.setForeground(defaultTextColor);
			resetAllDocumentStatistics.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			userSpecificMenuItems.add(resetAllDocumentStatistics);

			
			//User Event Listeners
			selectExistingUser.addActionListener((ActionEvent event) -> {
				System.out.println("Select Existing User Profile");
				UserSelectionFrame userSignIn = new UserSelectionFrame(ActionType.SIGN_IN, mainGUI);
			});
			createNewUser.addActionListener((ActionEvent event) -> {
				System.out.println("Create New User");
				UserSelectionFrame userCreate = new UserSelectionFrame(ActionType.CREATE_NEW, mainGUI);
			});
			resetAllDocumentStatistics.addActionListener((ActionEvent event) -> {
				System.out.println("Reset All Document Statistics");
				//TODO
			});
			
			userMenu.add(selectExistingUser);
			userMenu.add(createNewUser);
			userMenu.add(resetAllDocumentStatistics);
			this.add(userMenu);
	
			//Options Menu
			JMenu optionsMenu = new JMenu("Options");
			optionsMenu.setForeground(Color.WHITE);
			optionsMenu.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));

			
			JMenuItem setSpeedSetting = new JMenuItem("Set Speed Setting");
			setSpeedSetting.setForeground(defaultTextColor);
			setSpeedSetting.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			userSpecificMenuItems.add(setSpeedSetting);

			
			JMenuItem setDisplayFont = new JMenuItem("Set Display Font");
			setDisplayFont.setForeground(defaultTextColor);
			setDisplayFont.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			userSpecificMenuItems.add(setDisplayFont);

			
			JMenuItem setPointerType = new JMenuItem("Set Pointer Type");
			setPointerType.setForeground(defaultTextColor);
			setPointerType.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			userSpecificMenuItems.add(setPointerType);

			
			//Options Menu Listeners
			setSpeedSetting.addActionListener((ActionEvent event) -> {
				System.out.println("Set Speed Setting");
				//TODO
			});
			setDisplayFont.addActionListener((ActionEvent event) -> {
				System.out.println("Set Display Font");
				UpdateBaseStyle updateBaseStyle = new UpdateBaseStyle();
			});
			setPointerType.addActionListener((ActionEvent event) -> {
				System.out.println("Set Pointer Type");
				UpdateHighlight updateHighlight = new UpdateHighlight();
			});
			
			optionsMenu.add(setSpeedSetting);
			optionsMenu.add(setDisplayFont);
			optionsMenu.add(setPointerType);
			this.add(optionsMenu);
		}
	}
	private class DocumentListPanel extends JPanel{
		
		private static final long serialVersionUID = 1L;
		int width = (int)(WINDOW_WIDTH*.25);   //25% wide, 60% high
		int height = (int)(WINDOW_HEIGHT*.7);
		
		// DEBUG ONLY: Test repaint() functionality.
		/*private int debugTestIteration = 0;
		public void debugTestRepaint(){
			model.addElement("Test " + debugTestIteration++);
			docList.setModel(model);
		}*/
		
		private JLabel titleBox;
		
		DocumentListPanel(String title){
			super();

			setLayout(new BorderLayout());
			model = new DefaultListModel<String>();
			for (String s: documents) {
				model.addElement(s);
			}
//			docList = new JList<String>(model);
			
			//Panel Styling Elements
			setSize(width,height);  
			setBorder(BorderFactory.createMatteBorder(2, 0, 2, 2, defaultBackgroundColor));
			setBackground(Color.white);			
			setForeground(defaultTextColor);
			
			//Set Label
			titleBox = new JLabel(title);
			titleBox.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));

			
			
			//Set Styling Elements for titlebox
			titleBox.setSize(width,DPIScaling.scaleInt(50));
			titleBox.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, defaultBackgroundColor));
			titleBox.setBackground(Color.white);
			titleBox.setForeground(defaultTextColor);
			this.add(titleBox,BorderLayout.NORTH);
			
			//Populate the docList
			String[] docArray = documents.toArray(new String[0]);
			docList = new JList<String>(docArray);
			docList.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent me){
					if(me.getClickCount() == 2){
						System.out.println("Mouse double-clicked in docList.");
						int docIndex = docList.locationToIndex(me.getPoint());
						if(docIndex < 0){
							return;
						}
						String filePath = model.getElementAt(docIndex);
						ArrayList<Document> invalidDocuments = new ArrayList<Document>();
						for(Document userDoc : currUser.getDocs()){
							if(userDoc.getPath().equals(filePath)){
								try {
									updateDocList();
									displayDocument(userDoc);
									currDoc = userDoc;
									return;
								} catch(FileNotFoundException ex){
									currDoc = null;
									invalidDocuments.add(userDoc);
									setCurrUser(currUser);
									JOptionPane.showMessageDialog(null, "File not found. Removing from document list.");
									break;
								}
							}
						}
						for(Document invalidDoc : invalidDocuments){
							currUser.getDocs().remove(invalidDoc);
							setCurrUser(currUser);
						}
					}
				}
			});
			docList.setFont(new Font("Sans-Serif", Font.PLAIN, defaultFontSize));
			
			//Set Styling Elements for docList
			docList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			docList.setLayoutOrientation(JList.VERTICAL);
			docList.setVisibleRowCount(-1);		//don't select an entry
			docList.setBackground(Color.white);
			docList.setForeground(defaultTextColor);

			
			//Add scrollbar
			listScroller = new JScrollPane(docList);
			
			//listScroller Styling
			listScroller.setPreferredSize(new Dimension(width, height-DPIScaling.scaleInt(50)));
			listScroller.setMaximumSize(new Dimension(width, height-DPIScaling.scaleInt(50)));
			listScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

			this.add(listScroller,BorderLayout.WEST);
			
		}
		
		public void setTitle(String title){
			titleBox.setText(title);
			titleBox.revalidate();
			titleBox.repaint();
		}
		
	}
	private class DocumentStatisticsPanel extends JPanel{
		
		private static final long serialVersionUID = 1L;

		int width = (int)(WINDOW_WIDTH*.25);   //20% wide, 60% high
		int height = (int)(WINDOW_HEIGHT*.2);
		
		LocalDateTime currTime;
		JLabel values;
		int currTrainingTimeMin;
		int longestTrainingTimeMin;
		int fastestWPM;
		
		DocumentStatisticsPanel() {
			
			//Collect Current Statistics
			currTime = LocalDateTime.now();
			currTrainingTimeMin = (int)startTime.until(currTime, MINUTES);

			if (fastestWPMToday <= currWPM) fastestWPMToday = currWPM;
			System.out.println("DEBUG ONLY: TBD - Stats collection for Longest Training Time, Fastest WPM (These come from document obj)");
			System.out.println("                                   ^^^^^^^^^^^^^^^^^^^^^^^^^");
			System.out.println("                                   If you said goodbye to me tonight,");
			System.out.println("                                   There would still be music left to write.");
			System.out.println("                                   What else could I do?");
			System.out.println("                                   I'm so inspired by you.");
			System.out.println("                                   That hasn't happened for the longest time.");
			System.out.println("                                 --Billy Joel, \"The Longest Time\"");
			System.out.println("                                   (Sorry.)");
			
			//Panel Styling Elements
			setLayout(new BorderLayout());
			setPreferredSize(new Dimension(width, height-DPIScaling.scaleInt(50)));
			setMaximumSize(new Dimension(width, height-DPIScaling.scaleInt(50)));
			setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, defaultBackgroundColor));
			setBackground(Color.white);

			
			JLabel title = new JLabel("Document Statistics:");
			title.setForeground(defaultTextColor);
			title.setFont(new Font("Serif", Font.PLAIN, emphasisFontSize));
			title.setHorizontalTextPosition(JLabel.LEFT);
			
			/*String labelContents="<html><p>Today's Training Time:  "+currTrainingTimeMin+"m<br>"+
										"Longest Training Time:  "+longestTrainingTimeMin+"m<br>"+
										"Current WPM:         "+currentWPM+"WPM<br>"+
										"Fastest WPM Today:   "+fastestWPMToday+"WPM<br>"+
										"Fastest WPM:         "+fastestWPM+"WPM</p></html>";
			*/
			
			String labelContents="<html><p><font size="+DPIScaling.scaleInt(3)+">"+
										"Today's Training Time:  <br>"+
										"Longest Training Time:  <br>"+
										"Current WPM:         <br>"+
										"Fastest WPM Today:   <br>"+
										"Fastest WPM:         <br></p></html>";
			
			JLabel label = new JLabel(labelContents);
			label.setForeground(defaultTextColor);
			label.setHorizontalTextPosition(JLabel.LEFT);
			
			String valueContents="<html><p><font size="+DPIScaling.scaleInt(3)+">"+
											currTrainingTimeMin+" m<br>"+
											longestTrainingTimeMin+" m<br>"+
											currWPM+" WPM<br>"+
											fastestWPMToday+" WPM<br>"+
											fastestWPM+" WPM</p></html>";
			
			values = new JLabel(valueContents);
			//Create a composite border in order to get blue edge + margin
			Border border = BorderFactory.createMatteBorder(0, 0, 0, 2, defaultBackgroundColor);
			Border margin = new EmptyBorder(0,0,0,8);
			setBorder(new CompoundBorder(border, margin));
			
			values.setForeground(defaultTextColor);
			values.setHorizontalTextPosition(JLabel.RIGHT);
			
			add(title, BorderLayout.NORTH);
			add(label, BorderLayout.WEST);
			add(values, BorderLayout.EAST);
		}
		
		public void updateStats(){
			currTime = LocalDateTime.now();
			currTrainingTimeMin = (int)startTime.until(currTime, MINUTES);
			int currTrainingTimeSec = currTrainingTimeMin * 60;
			if(currTrainingTimeSec > currUser.getRecordTrainingSec()){
				currUser.setRecordTrainingSec(currTrainingTimeSec);
			}
			longestTrainingTimeMin = currUser.getRecordTrainingSec() / 60;
			currWPM = textHighlighter.getWPM();
			if(currWPM > fastestWPMToday){
				fastestWPMToday = currWPM;
			}
			if(currWPM > currUser.getRecordWPM()){
				currUser.setRecordWPM(currWPM);
			}
			fastestWPM = currUser.getRecordWPM();
			String valueContents="<html><p><font size="+DPIScaling.scaleInt(3)+">"+
											currTrainingTimeMin+" m<br>"+
											longestTrainingTimeMin+" m<br>"+
											currWPM+" WPM<br>"+
											fastestWPMToday+" WPM<br>"+
											fastestWPM+" WPM</p></html>";
			
			values.setText(valueContents);
			values.revalidate();
			values.repaint();
		}
		
	}
	
	private abstract class UpdateFormatting extends JFrame {
		
		private static final long serialVersionUID = 1L;
		
		protected final Pattern validateNumber = Pattern.compile("(\\d+)");
		protected final Pattern validateHexRGB = Pattern.compile("#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})");
		
		// BASE:
		// Set font
		// Set font size
		// Set font color
		
		// HIGHLIGHT:
		// Set italic
		// Set bold
		// Set underlined
		// Set highlighted
		// Set underline color (if possible)
		// Set highlight color
		// Set font color
		
		protected JPanel settingsPanel;
		
		public UpdateFormatting(){
			super();
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
			
			createSettingsPanel();
			add(settingsPanel);
			
			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
			JButton submitButton = new JButton("Confirm");
			JButton cancelButton = new JButton("Cancel");
			buttonPanel.add(submitButton);
			buttonPanel.add(cancelButton);
			submitButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					submitSettings();
				}
			});
			cancelButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			add(buttonPanel);
		}
		
		protected abstract void createSettingsPanel();
		
		protected abstract void submitSettings();
		
	}
	
	private class UpdateBaseStyle extends UpdateFormatting {
		
		private static final long serialVersionUID = 1L;
		
		private JLabel settingsLabel;
		private JTextField fontSize;
		private JTextField fontColor;
		
		public UpdateBaseStyle(){
			super();
			setSize(DPIScaling.scaleInt(350), DPIScaling.scaleInt(120));
			setTitle("Base Text-Formatting Options");
			setVisible(true);
		}
		
		protected void createSettingsPanel(){
			settingsPanel = new JPanel();
			// Set font (maybe)
			// Set font size
			// Set font color
			settingsLabel	= new JLabel("Base Document Formatting:");
			fontSize		= new JTextField("Font size");
			fontColor		= new JTextField("Font color (RGB hex value)");
			settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
			JPanel y0 = new JPanel(); y0.setLayout(new BoxLayout(y0, BoxLayout.X_AXIS));
			JPanel y1 = new JPanel(); y1.setLayout(new BoxLayout(y1, BoxLayout.X_AXIS));
			JPanel y2 = new JPanel(); y2.setLayout(new BoxLayout(y2, BoxLayout.X_AXIS));
			y0.add(settingsLabel);
			y1.add(fontSize);
			y2.add(fontColor);
			settingsPanel.add(y0);
			settingsPanel.add(y1);
			settingsPanel.add(y2);
		}
		
		protected void submitSettings(){
			Pointer pointer = currUser.getPointer();
			Matcher m;
			
			m = validateNumber.matcher(fontColor.getText());
			if(!m.find()){
				JOptionPane.showMessageDialog(null, "Invalid font size.");
				return;
			}
			int fontSize = Integer.parseInt(m.group(1));
			
			m = validateHexRGB.matcher(fontColor.getText());
			if(!m.find()){
				JOptionPane.showMessageDialog(null, "Invalid color code.");
				return;
			}
			int[] baseColorRGB = new int[3];
			baseColorRGB[0] = Integer.parseInt(m.group(1), 16);
			baseColorRGB[1] = Integer.parseInt(m.group(2), 16);
			baseColorRGB[2] = Integer.parseInt(m.group(3), 16);
			
			pointer.fontSize		= fontSize;
			pointer.baseColorRGB	= baseColorRGB;
			
			pointer.updateStyle();
		}
	}
	
	private class UpdateHighlight extends UpdateFormatting {
		
		private static final long serialVersionUID = 1L;
		
		private JLabel settingsLabel;
		private JCheckBox bold;
		private JCheckBox italic;
		private JCheckBox underline;
		private JCheckBox highlight;
		private JTextField highlightColor;
		private JTextField fontColor;
		
		public UpdateHighlight(){
			super();
			setSize(DPIScaling.scaleInt(350), DPIScaling.scaleInt(120));
			setTitle("Text-Highlighting Options");
			setVisible(true);
		}
		
		protected void createSettingsPanel(){
			settingsPanel = new JPanel();
			// Set italic
			// Set bold
			// Set underlined
			// Set highlighted
			// Set underline color (if possible)
			// Set highlight color
			// Set font color
			settingsLabel	= new JLabel("Formatting for Current Word:");
			bold			= new JCheckBox("Enable bold");
			italic			= new JCheckBox("Enable italic");
			underline		= new JCheckBox("Enable underline");
			highlight		= new JCheckBox("Enable highlight");
			highlightColor	= new JTextField("Highlight color (RGB hex value)");
			fontColor		= new JTextField("Font color (RGB hex value)");
			settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
			JPanel y0 = new JPanel(); y0.setLayout(new BoxLayout(y0, BoxLayout.X_AXIS));
			JPanel y1 = new JPanel(); y1.setLayout(new BoxLayout(y1, BoxLayout.X_AXIS));
			JPanel y2 = new JPanel(); y2.setLayout(new BoxLayout(y2, BoxLayout.X_AXIS));
			JPanel y3 = new JPanel(); y3.setLayout(new BoxLayout(y3, BoxLayout.X_AXIS));
			y0.add(settingsLabel);
			y1.add(bold); y1.add(italic); y1.add(underline); y1.add(highlight);
			y2.add(highlightColor);
			y3.add(fontColor);
			settingsPanel.add(y0);
			settingsPanel.add(y1);
			settingsPanel.add(y2);
			settingsPanel.add(y3);
		}
		
		protected void submitSettings(){
			Pointer pointer = currUser.getPointer();
			Matcher m;
			
			m = validateHexRGB.matcher(fontColor.getText());
			if(!m.find()){
				JOptionPane.showMessageDialog(null, "Invalid color code.");
				return;
			}
			int[] currentWordColorRGB = new int[3];
			currentWordColorRGB[0] = Integer.parseInt(m.group(1), 16);
			currentWordColorRGB[1] = Integer.parseInt(m.group(2), 16);
			currentWordColorRGB[2] = Integer.parseInt(m.group(3), 16);
			
			m = validateHexRGB.matcher(highlightColor.getText());
			if(!m.find()){
				JOptionPane.showMessageDialog(null, "Invalid color code.");
				return;
			}
			int[] highlightColorRGB = new int[3];
			highlightColorRGB[0] = Integer.parseInt(m.group(1), 16);
			highlightColorRGB[1] = Integer.parseInt(m.group(2), 16);
			highlightColorRGB[2] = Integer.parseInt(m.group(3), 16);
			
			pointer.currentWordColorRGB	= currentWordColorRGB;
			pointer.highlightColorRGB	= highlightColorRGB;
			pointer.enableBold			= bold.isSelected();
			pointer.enableItalic		= italic.isSelected();
			pointer.enableUnderline		= underline.isSelected();
			pointer.enableHighlight		= highlight.isSelected();
			
			pointer.updateStyle();
		}
	}
	
	private class OpenNewFile extends JFrame {
		
		private static final long serialVersionUID = 1L;
		
		JLabel enterFileName;
		JTextField fileName;
		JPanel inputFrame;
		JButton browseButton;
		JButton submitButton;
		
		public OpenNewFile() {
			super("Open New Document");
			setSize(DPIScaling.scaleInt(300), DPIScaling.scaleInt(200));
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
			enterFileName = new JLabel("New Document(*.txt): ");
			fileName = new JTextField(25);
			inputFrame = new JPanel();
			browseButton = new JButton("Browse");
			submitButton = new JButton("OK");
			submitButton.addActionListener(new ButtonListener());
			browseButton.addActionListener(new ButtonListener());
			
			inputFrame.add(enterFileName);
			inputFrame.add(fileName);
			inputFrame.add(browseButton);
			inputFrame.add(submitButton);
			add(inputFrame);
			
			setVisible(true);
		}
		
		private class ButtonListener implements ActionListener {
			
			public void actionPerformed(ActionEvent e){
				if(e.getSource() == browseButton){
					JFileChooser fc = new JFileChooser();
					if(fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
						try {
							fileName.setText(fc.getSelectedFile().getCanonicalPath());
						} catch(IOException ex){}
					}
				} else if(e.getSource() == submitButton){
					String filePath = fileName.getText();
					if(filePath.length() == 0){
						JOptionPane.showMessageDialog(null, "Please enter a file path.");
						return;
					}
					ArrayList<Document> invalidDocuments = new ArrayList<Document>();
					boolean docInUserList = false;
//					for(Document userDoc : currUser.getDocs()){
//						System.out.println(userDoc.getPath());
//					}
					for(Document userDoc : currUser.getDocs()){
						if(userDoc.getPath().equals(filePath)){
							docInUserList = true;
							try {
								updateDocList();
								displayDocument(userDoc);
								currDoc = userDoc;
								dispose();
								return;
							} catch(FileNotFoundException ex){
								currDoc = null;
								invalidDocuments.add(userDoc);
								setCurrUser(currUser);
								JOptionPane.showMessageDialog(null, "File not found. Removing from document list.");
								break;
							}
						}
					}
					for(Document invalidDoc : invalidDocuments){
						currUser.getDocs().remove(invalidDoc);
						setCurrUser(currUser);
					}
					if(docInUserList){
						return;
					}
					try {
						Document doc = new Document(filePath);
						addDoc(doc);
						displayDocument(doc);
						currDoc = doc;
						dispose();
					} catch(FileNotFoundException ex){
						currDoc = null;
						setCurrUser(currUser);
						JOptionPane.showMessageDialog(null, "File not found.");
						return;
					}
				}
			}
		}
	}
	
	public void updateStatsPanel(){
		docStatsPanel.updateStats();
		centerPanel.updateStats();
	}
	
}
