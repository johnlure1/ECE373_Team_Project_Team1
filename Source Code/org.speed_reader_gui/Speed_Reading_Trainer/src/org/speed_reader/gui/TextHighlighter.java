package org.speed_reader.gui;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

import org.speed_reader.data.Pointer;

public class TextHighlighter extends Thread {
	
	private static final int wpmIncrement = 5;
	
	private Style baseStyle;
	private Style highlight;
	private /*Default*/StyledDocument doc;
	private KeyCommandTextPane textPane;
	private ArrayList<int[]> wordDelimiters;
	private int[] docBounds;
	private int wordIdx;
	private int wpm;
	private boolean paused = true;
	private boolean exit = false;
	public volatile static boolean switchingThreads = false;
	
	public void togglePause(){
		paused ^= true; // Toggle paused flag.
		System.out.println("togglePause() called.");
	}
	
	public void setPaused(boolean paused){
		this.paused = paused;
	}
	
	public void decreaseReadingSpeed(){
		System.out.println("decreaseReadingSpeed() called.");
		// Adjust reading speed.
		wpm -= wpmIncrement;
	}
	
	public void increaseReadingSpeed(){
		System.out.println("increaseReadingSpeed() called.");
		// Adjust reading speed.
		wpm += wpmIncrement;
	}
	
	public TextHighlighter(StyledDocument doc, KeyCommandTextPane textPane, String docStr, Pointer pointer, int wpm){
		super();
		this.wpm = wpm;
		docBounds = new int[2];
		docBounds[0] = 0;
		docBounds[1] = docStr.length();
		// Prevent excessive reallocation by estimating word count.
		wordDelimiters = new ArrayList<int[]>(docStr.length() / 6);
		Pattern p = Pattern.compile("[\\w'�\\-]+");
		Matcher m = p.matcher(docStr);
		int lastWordStart = 0;
		int lastWordEnd = 0;
		while(m.find(lastWordEnd)){
			lastWordStart = m.start();
			lastWordEnd = m.end();
			int[] bounds = {lastWordStart, lastWordEnd - lastWordStart};
			wordDelimiters.add(bounds);
		}
		wordIdx = 0;
		
		this.doc = doc;
		baseStyle = pointer.getBaseStyle();
		highlight = pointer.getHighlight();
		this.textPane = textPane;
	}
	
	public TextHighlighter(StyledDocument doc, KeyCommandTextPane textPane, String docStr, Pointer pointer){
		this(doc, textPane, docStr, pointer, 300); // Default to 300 WPM.
	}
	
	public StyledDocument getStyledDocument(){
		return doc;
	}
	
	private void moveHighlight(int[] oldBounds, int[] newBounds){
		doc.setCharacterAttributes(oldBounds[0], oldBounds[1], baseStyle, true);
		doc.setCharacterAttributes(newBounds[0], newBounds[1], highlight, false);
		textPane.revalidate();
		textPane.repaint();
	}
	
	public void highlightWord(int newIdx) throws IndexOutOfBoundsException {
		moveHighlight(wordDelimiters.get(wordIdx), wordDelimiters.get(newIdx));
		wordIdx = newIdx;
	}
	
	public void highlightFirstWord(){
		try {
			highlightWord(0);
		} catch(IndexOutOfBoundsException e){
			wordIdx = 0;
		}
	}
	
	public void highlightNextWord() throws IndexOutOfBoundsException {
		highlightWord(wordIdx + 1);
	}
	
	@Override
	public void run(){
		try {
			highlightFirstWord();
		} catch(IndexOutOfBoundsException e){}
		setPaused(true);
		while(!exit){
			while(paused && !exit){
				System.out.println("Paused.");
				synchronized(this){
					System.out.println("TextHighlighter pause-loop iteration received lock.");
					try {
						this.wait();
						System.out.println("textHighlighter notified.");
					} catch(InterruptedException e){
						// Exit wait(), but continue pause loop unless pause has been set to false.
					}
				}
			}
			System.out.println("Not paused.");
			startReading();
		}
		synchronized(this){
			System.out.println("Exiting textHighlighter thread.");
			this.notify();
		}
	}
	
	private void startReading(){
		int msPerWord = 60000 / wpm; // (60000 ms/min) / (wpm words/min) = 60000/wpm ms/word
		if(msPerWord <= 0) throw new IllegalArgumentException();
		long msOld = System.currentTimeMillis();
		long msNew;
		while(true){
//			System.out.println("textHighlighter.startReading() waiting for lock.");
			if(switchingThreads){
				return;
			}
			synchronized(this){
//				System.out.println("textHighlighter.startReading() received lock.");
				try {
					msNew = System.currentTimeMillis();
					try {
						this.wait(msPerWord - (int)(msNew - msOld));
					} catch(InterruptedException e){
						return;
					} catch(IllegalArgumentException e){
						// (msPerWord - (int)(msNew - msOld)) was negative. Don't sleep at all.
					}
					msOld = System.currentTimeMillis();
					highlightNextWord();
				} catch(IndexOutOfBoundsException e){
					paused = true;
					return;
				}
			}
		}
	}
	
//	private void startReading(int wpm){
//		this.wpm = wpm;
//		startReading();
//	}
	
	public KeyCommandTextPane getTextPane(){
		return textPane;
	}
	
	public void setWPM(int wpm){
		this.wpm = wpm;
	}
	
	public int getWPM(){
		return wpm;
	}
	
	public void exit(){
		exit = true;
		interrupt();
	}

}
