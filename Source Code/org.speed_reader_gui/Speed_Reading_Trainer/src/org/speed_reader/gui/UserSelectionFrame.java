package org.speed_reader.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.speed_reader.data.User;

public class UserSelectionFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	public final ActionType action;
	
/*	// TEMPORARY testing method
	public static void main(String[] args){
		UserSelectionFrame login = new UserSelectionFrame(ActionType.CREATE_NEW);
//		UserSelectionFrame login = new UserSelectionFrame(ActionType.SIGN_IN);
		login.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}*/
	
	public UserSelectionFrame(ActionType action, MainGUI mainGUI){
		super();
		this.action = action;
		setTitle(this.action.titleText);
		UsernamePanel usernamePanel = new UsernamePanel(action);
		PasswordPanel passwordPanel = new PasswordPanel(action);
		ButtonPanel buttonPanel = new ButtonPanel(action, usernamePanel, passwordPanel, mainGUI);
		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		add(usernamePanel);
		add(passwordPanel);
		add(buttonPanel);
		setSize(DPIScaling.scaleInt(250), DPIScaling.scaleInt(100));
		setLocationRelativeTo(mainGUI);
		setVisible(true);
	}
	
	public enum ActionType {
		CREATE_NEW("New User", "Register"),
		SIGN_IN("Sign In", "Log In");
		public final String titleText;
		public final String buttonText;
		ActionType(String titleText, String buttonText){
			this.titleText = titleText;
			this.buttonText = buttonText;
		}
	}
	
	private class UsernamePanel extends JPanel {
		
		private static final long serialVersionUID = 1L;
		JLabel		usernameLabel;
		JTextField	usernameField;

		public UsernamePanel(ActionType action){
			super();
			usernameLabel = new JLabel("Username");
			usernameField = new JTextField(20);
			add(usernameLabel);
			add(usernameField);
		}
	}
	
	private class PasswordPanel extends JPanel {
		
		private static final long serialVersionUID = 1L;
		JLabel			passwordLabel;
		JPasswordField	passwordField;

		public PasswordPanel(ActionType action){
			super();
			passwordLabel = new JLabel("Password");
			passwordField = new JPasswordField(20);
			add(passwordLabel);
			add(passwordField);
		}
	}
	
	private class ButtonPanel extends JPanel {
		
		private static final long serialVersionUID = 1L;
		
		private ActionType action;
		private JButton submitButton;
		private JButton cancelButton;
		private UsernamePanel usernamePanel;
		private PasswordPanel passwordPanel;
		private MainGUI mainGUI;

		public ButtonPanel(
				ActionType action,
				UsernamePanel usernamePanel,
				PasswordPanel passwordPanel,
				MainGUI mainGUI)
		{
			super();
			this.action = action;
			this.usernamePanel = usernamePanel;
			this.passwordPanel = passwordPanel;
			this.mainGUI = mainGUI;
			submitButton = new JButton(action.buttonText);
			cancelButton = new JButton("Cancel");
			add(submitButton);
			add(cancelButton);
			submitButton.addActionListener(new ButtonListener());
			cancelButton.addActionListener(new ButtonListener());
		}
		
		private class ButtonListener implements ActionListener {
			
			public void actionPerformed(ActionEvent e){
				String username = usernamePanel.usernameField.getText();
				String password = new String(passwordPanel.passwordField.getPassword());
				if(e.getSource().equals(submitButton)){
					// System.out.println("\"" + action.buttonText + "\" button pressed.");
					if(action == ActionType.CREATE_NEW){
						boolean usernameTaken = false;
						for(User user : mainGUI.allUsers){
							if(user.getName().equals(username)){
								usernameTaken = true;
								JOptionPane.showMessageDialog(null, "A user already exists by that name.", "Username Unavailable", JOptionPane.ERROR_MESSAGE);
								break;
							}
						}
						if(!usernameTaken){
							User newUser = new User(username);
							if(newUser.setPassword(password)){
								mainGUI.allUsers.add(newUser);
								mainGUI.setCurrUser(newUser);
								dispose();
							} else {
								JOptionPane.showMessageDialog(null,
										"Please use at least eight characters, including characters from at least three of the following categories:\n"+
										"Lower-case alphabetical\n"+
										"Upper-case alphabetical\n"+
										"Numeric\n"+
										"Symbolic\n"+
										"Whitespace\n",
										"Weak Password",
										JOptionPane.ERROR_MESSAGE);
							}
						}
					} else if(action == ActionType.SIGN_IN){
						User userByName = null;
						for(User user : mainGUI.allUsers){
							if(user.getName().equals(username)){
								userByName = user;
								break;
							}
						}
						if(userByName != null){
							if(userByName.checkPassword(password)){
								mainGUI.setCurrUser(userByName);
								dispose();
							} else {
								JOptionPane.showMessageDialog(null, "You typed an incorrect password.", "Invalid Password", JOptionPane.ERROR_MESSAGE);
							}
						} else {
							JOptionPane.showMessageDialog(null, "No user exists by that name.", "Invalid Username", JOptionPane.ERROR_MESSAGE);
						}
					}
				} else if(e.getSource().equals(cancelButton)){
					// System.out.println("\"Cancel\" button pressed.");
					dispose();
				}
			}
			
		}
		
	}
	
}
