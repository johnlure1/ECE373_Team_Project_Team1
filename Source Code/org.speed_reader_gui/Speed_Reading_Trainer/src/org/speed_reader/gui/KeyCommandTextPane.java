package org.speed_reader.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

public class KeyCommandTextPane extends JTextPane implements KeyListener {
	
	private static final long serialVersionUID = 1L;
	
	private TextHighlighter textHighlighter;
	private MainGUI mainGUI;

	public KeyCommandTextPane(){
		super();
		addKeyListener(this);
	}
	
	public KeyCommandTextPane(StyledDocument doc){
		super(doc);
		addKeyListener(this);
	}
	
	public void setTextHighlighter(TextHighlighter textHighlighter){
		this.textHighlighter = textHighlighter;
	}
	
	public void setParentGUI(MainGUI mainGUI){
		this.mainGUI = mainGUI;
	}
	
	@Override
	public void keyPressed(KeyEvent ke){}
	
	@Override
	public void keyReleased(KeyEvent ke){}
	
	@Override
	public void keyTyped(KeyEvent ke){
		char keyChar = ke.getKeyChar();
		switch(keyChar){
		case ' ':
			synchronized(textHighlighter){
				System.out.println("textPane.keyTyped(KeyEvent ke) received lock.");
				textHighlighter.togglePause();
				textHighlighter.interrupt();
			}
			break;
		case ',':
		case '<':
			synchronized(textHighlighter){
				System.out.println("textPane.keyTyped(KeyEvent ke) received lock.");
				textHighlighter.decreaseReadingSpeed();
				textHighlighter.interrupt();
			}
			break;
		case '.':
		case '>':
			synchronized(textHighlighter){
				System.out.println("textPane.keyTyped(KeyEvent ke) received lock.");
				textHighlighter.increaseReadingSpeed();
				textHighlighter.interrupt();
			}
			break;
		default:
			break;
		}
		mainGUI.updateStatsPanel();
	}
}
