package org.speed_reader.data;

import java.awt.Color;
import java.io.Serializable;

import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import org.speed_reader.gui.DPIScaling;

public class Pointer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private StyleContext sc;
	private Style baseStyle;
	private Style highlight;
	
	public int fontSize = DPIScaling.scaleInt(12);
	public int[] baseColorRGB = {0, 0, 0};
	public int[] currentWordColorRGB = {255, 255, 255};
	public int[] highlightColorRGB = {0, 0, 255};
	public boolean enableBold = false;
	public boolean enableItalic = false;
	public boolean enableUnderline = false;
	public boolean enableHighlight = true;
	
	public Pointer(){
		sc = StyleContext.getDefaultStyleContext();
		baseStyle = sc.addStyle("Base Style", null);
		highlight = sc.addStyle("Highlight", null);
		updateStyle();
	}
	
	public void updateStyle(){
		baseStyle.addAttribute(StyleConstants.Size, fontSize);
		baseStyle.addAttribute(StyleConstants.Foreground, getColor(baseColorRGB));
		highlight.addAttribute(StyleConstants.Foreground, getColor(currentWordColorRGB));
		if(enableHighlight){
			highlight.addAttribute(StyleConstants.Background, getColor(highlightColorRGB));
		} else {
			highlight.removeAttribute(StyleConstants.Background);
		}
		highlight.addAttribute(StyleConstants.Bold, enableBold);
		highlight.addAttribute(StyleConstants.Italic, enableItalic);
		highlight.addAttribute(StyleConstants.Underline, enableUnderline);
	}
	
	public StyleContext getStyleContext(){
		return sc;
	}
	
	public Style getBaseStyle(){
		return baseStyle;
	}
	
	public Style getHighlight(){
		return highlight;
	}
	
	private static Color getColor(int[] rgb){
		return new Color(rgb[0], rgb[1], rgb[2]);
	}

}
